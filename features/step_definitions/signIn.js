import { By, until, Key } from "selenium-webdriver";
import { Given, When, Then } from "cucumber";

Given("Open sign in page", function () {
  return this.driver.get("http://localhost:3000/");
});

When("Fulfill user name and password", async function () {
  const world = this;
  const data = "testing@nexus.it";
  //username
  const userElement = await world.driver
    .findElement(By.css('[data-qa="email-field"]'))
    .findElement(By.name("email"));
  await world.driver.wait(until.elementIsVisible(userElement));
  await userElement.sendKeys(data);
  //password
  const passwordElement = await world.driver
    .findElement(By.css('[data-qa="password-field"]'))
    .findElement(By.name("password"));
  await world.driver.wait(until.elementIsVisible(passwordElement));
  await passwordElement.sendKeys(data);
});

When("Click sign in", async function () {
  const world = this;
  //submit button
  const element = await world.driver.findElement(
    By.css('[data-qa="submit-button"]')
  );
  await world.driver.wait(until.elementIsVisible(element));
  // await element.click();
});
