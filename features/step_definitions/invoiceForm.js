import { By, until, Key } from "selenium-webdriver";
import { Given, When, Then } from "cucumber";

Given("Open invoice form", function () {
  return this.driver.get("http://localhost:3000/invoices/new");
});


When("Fulfill invoice form", async function () {
  const world = this;
  await world.driver.sleep(2000);
  //submit button
  const number = await world.driver.findElement(
      By.css('[data-qa="number"]')
  ).findElement(By.name('number'));

  const saleDateInput = await world.driver.findElement(
    By.css('[data-qa="saleDateInputWrapper"]')
  ).findElement(By.css(".MuiInputBase-input"));

  const dateOfInvoceCreation = await world.driver.findElement(
    By.css('[data-qa="dateOfInvoceCreation"]')
  ).findElement(By.css(".MuiInputBase-input"));

  const dealer = await world.driver
      .findElement(By.css('[data-qa="dealer"]'))
      .findElement(By.css("option[value='898-207-69-47']"));

  const customer = await world.driver
      .findElement(By.css('[data-qa="customer"]'))
      .findElement(By.css("option[value='test']"));
  // const paymentMethod = await world.driver.findElement(
  //   By.css('[data-qa="paymentMethod"]')
  // );
  // const paymentDate = await world.driver.findElement(
  //   By.css('[data-qa="paymentDate"]')
  // );
  // const wholeNett = await world.driver.findElement(
  //   By.css('[data-qa="wholeNett"]')
  // );
  // const wholeVatAmount = await world.driver.findElement(
  //   By.css('[data-qa="wholeVatAmount"]')
  // );
  // const wholeGross = await world.driver.findElement(
  //   By.css('[data-qa="wholeGross"]')
  // );
  // const saveButton = await world.driver.findElement(
  //   By.css('[data-qa="saveButton"]')
  // );
  // const more = await world.driver.findElement(
  //   By.css('[data-qa="more"]').findElement(By.name('more'));
  // );

  await world.driver.wait(until.elementIsVisible(number));
  await number.sendKeys(Key.chord(Key.COMMAND, 'a'), "FV 4-10-2021");

  await world.driver.wait(until.elementIsVisible(saleDateInput));
  await saleDateInput.sendKeys(Key.chord(Key.COMMAND, 'a'), "16/11/2021");

  await world.driver.wait(until.elementIsVisible(dateOfInvoceCreation));
  await dateOfInvoceCreation.sendKeys("20/11/2021");

  await world.driver.wait(until.elementIsVisible(dealer));
  await dealer.click();

  await world.driver.wait(until.elementIsVisible(customer));
  await customer.click();
  //
  // await world.driver.wait(until.elementIsVisible(dateOfInvoceCreation));
  // await dateOfInvoceCreation.sendKeys("16/11/2021")
  // await element.click();
});
