import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import DescriptionOutlined from "@mui/icons-material/DescriptionOutlined";
import { PeopleOutline, Settings } from "@mui/icons-material";
import { Link, Switch, Route } from "react-router-dom";
import { styled } from "@mui/material/styles";
import { Fab } from "@mui/material";

const StyledFab = styled(Fab)({
  position: "absolute",
  zIndex: 1,
  top: -30,
  left: 0,
  right: 0,
  margin: "0 auto",
});

const StyledLink = styled(Link)({
  color: "white",
  display: "flex",
});
export default function BottomMenu() {
  return (
    <AppBar position="fixed" color="primary" sx={{ top: "auto", bottom: 0 }}>
      <Switch>
        <Route path="/invoices">
          <StyledLink to="/invoices/new">
            <StyledFab color="secondary" aria-label="add">
              <AddIcon />
            </StyledFab>
          </StyledLink>
        </Route>
        <Route path="/clients">
          <StyledLink to="/clients/new">
            <StyledFab color="secondary" aria-label="add">
              <AddIcon />
            </StyledFab>
          </StyledLink>
        </Route>
        <Route path="/data">
          <StyledLink to="/data/new">
            <StyledFab color="secondary" aria-label="add">
              <AddIcon />
            </StyledFab>
          </StyledLink>
        </Route>
      </Switch>

      <Toolbar sx={{ display: "flexs", justifyContent: "space-between" }}>
        <IconButton color="inherit" aria-label="open drawer">
          <StyledLink to="/invoices">
            <DescriptionOutlined />
          </StyledLink>
        </IconButton>
        <IconButton color="inherit">
          <StyledLink to="/clients">
            <PeopleOutline />
          </StyledLink>
        </IconButton>
        <IconButton color="inherit">
          <StyledLink to="/data">
            <Settings />
          </StyledLink>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
