import { Grid, Select } from "@mui/material";
import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import { Invoice } from "../container/InvoiceForm/type";

interface Props {
  data: any;
  id: number;

  fieldUpdate: any;
}
export default function InvoiceList(props: Props) {
  const [dense, setDense] = useState(true);
  console.log(props.data);
  return (
    <ListItem
      id={`${props.id}`}
      secondaryAction={
        <IconButton edge="end" aria-label="delete">
          <DeleteIcon />
        </IconButton>
      }
    >
      <Grid item xs={12} sm={2}>
        <TextField
          id="field"
          label="Towar lub usługa"
          variant="outlined"
          value={props.data.name}
          onChange={props.fieldUpdate}
        />
      </Grid>
      <Grid item xs={12} sm={1}>
        <TextField
          id="amount"
          label="Ilość"
          value={props.data.amount}
          type="number"
          variant="outlined"
          onChange={props.fieldUpdate}
        />
      </Grid>
      <Grid item xs={12} sm={1}>
        <Select
          labelId="demo-simple-select-label"
          id="jm"
          value={"sztuka"}
          onChange={props.fieldUpdate}
          label={"jm"}
          native
        >
          <option value={"godz"}>godzina</option>
          <option value={"sztuka"}>sztuka</option>
        </Select>
      </Grid>
      <Grid item xs={12} sm={2}>
        <TextField
          id="value"
          label="Cena netto"
          type="number"
          value={props.data.value}
          variant="outlined"
          onChange={props.fieldUpdate}
        />
      </Grid>
      <Grid item xs={12} sm={2}>
        <TextField
          id="sumNett"
          label="Wartość netto"
          type="number"
          value={props.data.sumNett}
          variant="outlined"
          disabled
        />
      </Grid>
      <Grid item xs={12} sm={1}>
        <TextField
          id="vat"
          label="Vat"
          type="number"
          value={props.data.vat}
          variant="outlined"
          onChange={props.fieldUpdate}
        />
      </Grid>
      <Grid item xs={12} sm={1}>
        <TextField
          id="vatAmount"
          label="Kwota Vat"
          type="number"
          value={props.data.vatAmount}
          variant="outlined"
          disabled
        />
      </Grid>
      <Grid item xs={12} sm={2}>
        <TextField
          id="sumWithVat"
          label="Brutto"
          type="number"
          value={props.data.sumWithVat}
          variant="outlined"
          disabled
        />
      </Grid>
    </ListItem>
  );
}
