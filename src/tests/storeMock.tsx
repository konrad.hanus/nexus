import { AnyAction, createStore, Store } from "redux";
import React from "react";
import rootReducer from "../store/rootReducer";
import { mount, shallow } from "enzyme";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore([]);
export const storeFactory = (initialState) => {
  const store = mockStore(initialState);
  return store;
};

export const shallowComponent = (
  Component: any,
  store: Store<any, AnyAction>
) => {
  return shallow(<Component store={store} />);
};

export const mountComponent = (
  Component: any,
  store: Store<any, AnyAction>
) => {
  return mount(<Component store={store} />);
};
