import { Factory } from "fishery";
import { Element, Invoice } from "../../container/InvoiceForm/type";

export default Factory.define<Element>(({ sequence }) => ({
  amount: 2,
  value: 2,
  sumNett: 0,
  sumWithVat: 0,
  vat: 23,
  vatAmount: 0,
  name: "",
  jm: "",
}));
