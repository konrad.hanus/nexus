import { Factory } from "fishery";
import { Invoice } from "../../container/InvoiceForm/type";
import elementFactory from "./ElementFactory";

export default Factory.define<Invoice>(({ sequence }) => ({
  number: `FV${sequence}Test`,
  saleDate: "",
  wholeVat: 0,
  wholeNett: 0,
  wholeVatAmount: 0,
  wholeGross: 0,
  client: "TEST",
  myCompany: "Test",
  paymentDate: "",
  paymentMethod: "",
  more: "",
  dateOfInvoceCreation: {},
  elements: elementFactory.buildList(5),
}));
