import React from "react";
import { shallow } from "enzyme";
import SignIn from "../../container/SignIn/index";
import { shallowComponent, storeFactory } from "../storeMock";
import { Provider } from "react-redux";

const initialState = {
  user: { data: { user: "test" } },
};

describe("SignIn", () => {
  it("should wrap component in store", () => {
    const store = storeFactory(initialState);
    const wrapper = shallowComponent(SignIn, store);
    expect(wrapper.getElements()).toMatchSnapshot();
  });
});
