import React from "react";
import { mountComponent, shallowComponent, storeFactory } from "../storeMock";
import ClientForm from "../../container/ClientForm/index";
import { TextField } from "@mui/material";
const initialState = {
  user: { data: { user: "test" } },
  clientForm: {},
};
let store;
beforeAll(() => {
  store = storeFactory(initialState);
});

describe("ClientForm", () => {
  it("should wrap component in store", () => {
    const wrapper = shallowComponent(ClientForm, store);
    expect(wrapper.getElements()).toMatchSnapshot();
  });
  it("should render componenet", () => {
    const mounted = mountComponent(ClientForm, store);
    const form = mounted.find(TextField);
  });
});
