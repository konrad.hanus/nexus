import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import InvoicesPage from "./page/InvoicesPage";
import ClientsPage from "./page/ClientsPage";
import DataPage from "./page/DataPage";
import NewPage from "./page/NewPage";
import BottomMenu from "./routes/BottomMenu";
import LoginPage from "./page/LoginPage";
import InvoicePage from "./page/InvoicePage";
export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:page/new">
          <NewPage />
          <BottomMenu />
        </Route>
        <Route path="/invoice/:number">
          <InvoicePage></InvoicePage>
        </Route>
        <Route path="/invoices">
          <InvoicesPage />
          <BottomMenu />
        </Route>
        <Route path="/clients">
          <ClientsPage />
          <BottomMenu />
        </Route>
        <Route path="/data">
          <DataPage />
          <BottomMenu />
        </Route>
        <Route path="/">
          <LoginPage />
        </Route>
      </Switch>
    </Router>
  );
}
