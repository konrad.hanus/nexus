export interface RouteParams {
  page: string;
  number: string;
}
