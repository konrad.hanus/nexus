import sha256 from "crypto-js/sha256";

class Block {
  id: number;
  timestamp: number;
  data: any;
  previousHash: string;
  hash: string;

  constructor(id, timestamp, data, previousHash = "") {
    this.id = id;
    this.timestamp = timestamp;
    this.data = data;
    this.previousHash = previousHash;
    this.hash = this.generateHash();
  }

  generateHash() {
    const hash = sha256(
      this.id +
        this.timestamp +
        JSON.stringify(this.data).toString() +
        this.previousHash
    );
    return hash.toString();
  }
}

class BlockChain {
  readonly chain: Array<Block>;
  constructor() {
    this.chain = [this.createFirstBlock()];
  }

  createFirstBlock() {
    return new Block(0, "1635844257", { name: "first Invoice" }, "0");
  }

  getLastBlock() {
    return this.chain[this.chain.length - 1];
  }

  addBlock(newBlock: Block) {
    newBlock.previousHash = this.getLastBlock().hash;
    newBlock.hash = newBlock.generateHash();
    this.chain.push(newBlock);
  }

  isChainValid() {
    for (let i = 1; i < this.chain.length; i++) {
      const currBlock = this.chain[i];
      const prevBlock = this.chain[i - 1];

      if (currBlock.hash !== currBlock.generateHash()) {
        return false;
      }

      if (currBlock.previousHash !== prevBlock.hash) {
        return false;
      }
    }
    return true;
  }
}

const nexusCoin = new BlockChain();
nexusCoin.addBlock(
  new Block(1, new Date().getTime(), {
    id: 1,
    name: "FV 001/2021",
    amount: 100000,
  })
);
nexusCoin.addBlock(
  new Block(1, new Date().getTime(), {
    id: 2,
    name: "FV 002/2021",
    amount: 200000,
  })
);

nexusCoin.addBlock(
  new Block(1, new Date().getTime(), {
    id: 3,
    name: "FV 003/2021",
    amount: 300000,
  })
);
nexusCoin.addBlock(
  new Block(1, new Date().getTime(), {
    id: 4,
    name: "FV 004/2021",
    amount: 400000,
  })
);

console.log("is valid?:", nexusCoin.isChainValid());

console.log("is valid?:", nexusCoin.isChainValid());

export default nexusCoin;
