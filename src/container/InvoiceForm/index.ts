import InvoiceForm from "./InvoiceForm";
import { actionCreators } from "./action";
import { connect } from "react-redux";
import { actionCreators as clientActionCreator } from './../ClientForm/action';

function mapStateToProps({ invoiceForm, clientForm, data }) {
  return {
    nextValue: invoiceForm.nextValue,
    clients: clientForm.list,
    data: data.list,
  };
}

export default connect(mapStateToProps, {...clientActionCreator, ...actionCreators})(InvoiceForm);
