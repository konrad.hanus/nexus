import { INVOICE_PAGE_ACTIONS } from "../../page/InvoicesPage/type";
import { INVOICE_ACTIONS } from "./type";

const reducer = (state = { list: [], nextValue: 1 }, action) => {
  switch (action.type) {
    case INVOICE_ACTIONS.CREATE_INVOICE:
      return {
        ...state,
        list: [...state.list, action.payload],
      };
      break;
    case INVOICE_PAGE_ACTIONS.SET_INVOICES:
      return {
        ...state,
        list: [...Object.values(action.payload)],
        nextValue: ++Object.values(action.payload).length,
      };
      break;
    default:
      return state;
  }
};

export default reducer;
