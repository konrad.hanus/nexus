import {
  Invoice,
  INVOICE_ACTIONS,
  CreateNewInvoiceAction,
  IncrementAction,
} from "./type";

export const actionCreators = {
  createNewInvoice: (data: any): CreateNewInvoiceAction => ({
    type: INVOICE_ACTIONS.CREATE_INVOICE,
    payload: data,
  }),
  incrementNextValue: (nextValue: number): IncrementAction => ({
    type: INVOICE_ACTIONS.INCREMENT,
    payload: nextValue,
  }),
};
