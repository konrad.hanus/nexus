import { takeEvery, all, fork, put, select, call } from "redux-saga/effects";
import { actionCreators } from "./action";
import { INVOICE_ACTIONS } from "./type";
import { getNextValue } from "./selector";
import { reduxSagaFirebase } from "../../firebase";
import { INVOICE_PAGE_ACTIONS } from "../../page/InvoicesPage/type";

export function* onSaveSaga(action) {
  const next = yield select(getNextValue);
  yield call(reduxSagaFirebase.database.create, "invoice", action.payload);
  yield put(actionCreators.incrementNextValue(next));
  const data = yield call(fetch, "https://api2.binance.com/api/v3/ticker/24hr");
}

function* saveSaga() {
  yield takeEvery(INVOICE_ACTIONS.CREATE_INVOICE, onSaveSaga);
}

export function* invoceFormSaga() {
  yield all([fork(saveSaga)]);
}
