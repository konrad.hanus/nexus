import {
  Button,
  FormControl,
  FormHelperText,
  Grid,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import InvoiceList from "../../component/InvoiceList";
import { useEffect } from "react";
import { CreateNewInvoiceAction } from "./type";

import React, { useState } from "react";
import { actionCreators } from "./action";
import { useHistory } from "react-router";
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { makeStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import ControlPointIcon from "@mui/icons-material/ControlPoint";
import DatePicker from "@mui/lab/DatePicker";
import { actionCreators as clientActionCreator } from "./../ClientForm/action";

interface Props {
  nextValue: number;
  clients: Array<any>;
  data: Array<any>;
}
type InvoiceActions = typeof actionCreators;
type ClientActions = typeof clientActionCreator;

type Actions = InvoiceActions & ClientActions;

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  marginTop: 15,
  color: theme.palette.text.secondary,
}));

const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
  },
  right: {
    display: "flex",
    justifyContent: "flex-end",
  },
});

export default function InvoiceForm(props: Props & Actions) {
  const date = new Date();
  const empty = {
    amount: 0,
    value: 0,
    sumNett: 0,
    sumWithVat: 0,
    vat: 23,
    vatAmount: 0,
    name: "",
    jm: "",
  };

  useEffect(() => {
    props.getClients();
  }, []);
  const hasClients = props.clients !== [] && props.clients;

  const [invoice, setInvoice] = useState({
    number: `FV ${props.nextValue}-${date.getMonth()}-${date.getFullYear()}`,
    saleDate: 0,
    wholeVat: 0,
    wholeNett: 0,
    wholeVatAmount: 0,
    wholeGross: 0,
    paymentDate: "",
    dateOfInvoceCreation: "",
    elements: [
      {
        ...empty,
      },
    ],
    dealer: props.data[0].nip,
    customer: undefined,
  });

  const [fields, setFields] = useState([0]);
  const [dense, setDense] = useState(true);
  const [secondary, setSecondary] = useState(false);

  const history = useHistory();
  const changeHandler = (e: any) => {
    let tmp = { ...invoice };
    tmp[e.target.id] = e.target.value;
    setInvoice(tmp);
  };

  const filedTowarUsugla = (e: any) => {
    // setInvoice({ ...invoice, elements: list });
  };

  const fieldUpdate = (e: any) => {
    const list = [...invoice.elements];

    const id =
      e.target.parentElement.parentElement.parentElement.parentElement.id ||
      e.target.parentElement.parentElement.parentElement.id;

    const curr = list[id];
    if (e.target.id === "field") {
      curr.name = e.target.value;
    } else {
      curr[e.target.id] = e.target.value;
    }
    if (
      e.target.id === "amount" ||
      e.target.id === "vat" ||
      e.target.id === "value"
    ) {
      curr.sumNett = curr.amount * curr.value;
      curr.vatAmount = curr.sumNett * (curr.vat / 100);
      curr.sumWithVat = curr.sumNett + curr.vatAmount;
    }
    const summaryNett = list
      .map((elem) => elem.sumNett)
      .reduce((p, c) => {
        return p + c;
      });

    const summaryVatAmount = list
      .map((elem) => elem.vatAmount)
      .reduce((p, c) => {
        return p + c;
      });

    const summaryGross = list
      .map((elem) => elem.sumWithVat)
      .reduce((p, c) => {
        return p + c;
      });

    setInvoice({
      ...invoice,
      elements: list,
      wholeGross: summaryGross,
      wholeNett: summaryNett,
      wholeVatAmount: summaryVatAmount,
    });
  };
  const addElement = () => {
    const list = [...invoice.elements];
    list.push({ ...empty });
    setInvoice({ ...invoice, elements: list });
    const tmp = fields;
    let last = fields[fields.length - 1];
    tmp.push(++last);
    setFields([...tmp]);
  };
  const clickHandler = () => {
    props.createNewInvoice(invoice);
    history.push("/invoices");
  };

  const datepickerHandler = (field) => {
    return (date) => {
      setInvoice({
        ...invoice,
        [field]: date.toString(),
      });
    };
  };

  const classes = useStyles();

  const Demo = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.background.paper,
  }));

  return (
    <Container>
      <Item>
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.root}>
            Nowa faktura
          </Grid>
          <Grid item xs={12} sm={12} className={classes.right}>
            <TextField
              data-qa="number"
              name="number"
              id="number"
              label="Numer"
              variant="outlined"
              value={invoice.number}
              onChange={changeHandler}
            />
          </Grid>
          <Grid item xs={12} sm={12} className={classes.right}>
            <div data-qa="saleDateInputWrapper">
              <DatePicker
                label="Data sprzedaży"
                value={invoice.saleDate}
                onChange={datepickerHandler("saleDate")}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </Grid>

          <Grid item xs={12} sm={12} className={classes.right}>
            <div data-qa="dateOfInvoceCreation">
              <DatePicker
                data-qa="dateOfInvoceCreation"
                label="Data wystawienia"
                value={invoice.dateOfInvoceCreation}
                onChange={datepickerHandler("dateOfInvoceCreation")}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <InputLabel id="demo-simple-select-label">Sprzedawca</InputLabel>
            <Select
              data-qa="dealer"
              labelId="demo-simple-select-label"
              id="dealer"
              value={invoice.dealer}
              onChange={changeHandler}
              label={"myCompany"}
              native
            >
              <option>Wybierz</option>
              {props.data.map((row) => (
                <option key={`${row.nip}-${Math.random()}`} value={row.nip}>
                  {row.name}
                </option>
              ))}
            </Select>
          </Grid>
          <Grid item xs={12} sm={6}>
            <InputLabel id="demo-simple-select-label">Nabywca</InputLabel>
            <Select
              data-qa="customer"
              labelId="demo-simple-select-label"
              id="customer"
              value={invoice.customer}
              onChange={changeHandler}
              label={"client"}
              native
            >
              <option>Wybierz</option>
              {hasClients &&
                props.clients.map((row) => (
                  <option
                    key={`${row.purchaserNip}-${Math.random()}`}
                    value={row.purchaserNip}
                  >
                    {row.purchaserName}
                  </option>
                ))}
            </Select>
          </Grid>

          <Grid item xs={11} md={11}>
            <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
              Lista
            </Typography>

            <Demo>
              <List dense={dense} key={Math.random()}>
                {fields.map((value, key) => {
                  const elements = [...invoice.elements];
                  const curr = { ...elements[value] };

                  return (
                    //Still reloaded on every state change. WHY?!
                    <InvoiceList
                      id={value}
                      key={key}
                      fieldUpdate={fieldUpdate}
                      data={curr}
                    />
                  );
                })}
              </List>
            </Demo>
          </Grid>
          <Grid item xs={1} sm={1}>
            <ControlPointIcon onClick={addElement} />
          </Grid>

          <Grid item xs={12} sm={12}></Grid>

          <Grid item xs={12} sm={6}></Grid>
        </Grid>
      </Item>

      <Item>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            Dodatkowe
          </Grid>

          <Grid item xs={12} sm={6}>
            <InputLabel id="demo-simple-select-label">
              Sposób płatności
            </InputLabel>
            <Select
              data-qa="paymentMethod"
              labelId="demo-simple-select-label"
              id="paymentMethod"
              onChange={changeHandler}
              native
            >
              <option value={"basic"}>wybierz</option>
              <option value={"gotowka"}>gotówka</option>
              <option value={"przelew"}>przelew</option>
            </Select>
            <DatePicker
              data-qa="paymentDate"
              label="Termin płatności"
              value={invoice.paymentDate}
              onChange={datepickerHandler("paymentDate")}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              data-qa="wholeNett"
              id="wholeNett"
              label="Wartość netto"
              variant="outlined"
              disabled
              value={invoice.wholeNett}
            />
            <TextField
              data-qa="wholeVatAmount"
              id="wholeVatAmount"
              label="Kwota VAT"
              variant="outlined"
              disabled
              value={invoice.wholeVatAmount}
            />
            <TextField
              data-qa="wholeGross"
              id="wholeGross"
              label="Wartość brutto"
              variant="outlined"
              disabled
              value={invoice.wholeGross}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              data-qa="saveButton"
              id="more"
              name="more"
              label="Uwagi"
              variant="outlined"
              onChange={changeHandler}
            />
          </Grid>
        </Grid>
      </Item>
      <Grid item xs={12} sm={6}>
        <Button variant="contained" onClick={clickHandler} data-qa="saveButton">
          Zapisz
        </Button>
      </Grid>
    </Container>
  );
}
