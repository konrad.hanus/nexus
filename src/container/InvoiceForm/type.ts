export interface Invoice {
  client: string;
  dateOfInvoceCreation: any;
  elements: Array<Element>;
  more: string;
  myCompany: string;
  number: string;
  paymentDate: string;
  paymentMethod: string;
  saleDate: string;
  wholeGross: number;
  wholeNett: number;
  wholeVat: number;
  wholeVatAmount: number;
}
export interface Element {}
export enum INVOICE_ACTIONS {
  CREATE_INVOICE = "@INVOICE_FORM/CREATE",
  INCREMENT = "@INVOICE_FORM/INCREMENT",
}

export interface CreateNewInvoiceAction {
  type: INVOICE_ACTIONS.CREATE_INVOICE;
  payload: Invoice;
}

export interface IncrementAction {
  type: INVOICE_ACTIONS.INCREMENT;
  payload: number;
}
