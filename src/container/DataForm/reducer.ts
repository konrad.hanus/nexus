const initialState = [
  {
    name: "Mateusz Gibuła Bakkchos",
    nip: "8691882450",
    address: "Niedzieliska 283",
    city: "Szczurowa",
    postal: "32-820",
    bankAccount: "empty",
  },
  {
    name: "Hanus Commercial Arts - Konrad Hanus",
    nip: "898-207-69-47",
    address: "Piastowska 38/20",
    city: "Wrocław",
    postal: "50-361",
    bankAccount: "empty",
  },
];

const reducer = (state = { list: initialState }, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default reducer;
