import { connect } from "react-redux";
import { actionCreators } from "./action";
import SignIn from "./SignIn";

function mapStateToProps({ user }) {
  return { user: user.data.user };
}

export default connect(mapStateToProps, actionCreators)(SignIn);
