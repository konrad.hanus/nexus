import { AuthenticationActions } from "./type";
const reducer = (state = { data: {} }, action) => {
  switch (action.type) {
    case AuthenticationActions.SET_USER:
      return {
        ...state,
        data: action.payload,
      };
      break;
    default:
      return state;
  }
};

export default reducer;
