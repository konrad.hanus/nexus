import { takeEvery, all, fork, put, select, call } from "redux-saga/effects";
import { AuthenticationActions } from "./type";
import { reduxSagaFirebase } from "../../firebase";
import { actionCreators } from "./action";

function* onSignInSaga(action) {
  try {
    const user = yield call(
      reduxSagaFirebase.auth.signInWithEmailAndPassword,
      action.payload.email,
      action.payload.password
    );
    yield put(actionCreators.setUser(user));
    localStorage.setItem("user", JSON.stringify(user));

    console.log(user);
  } catch (e) {
    console.log(e);
  }
}

function* signInSaga() {
  yield takeEvery(AuthenticationActions.SIGN_IN, onSignInSaga);
}

export function* authSaga() {
  yield all([fork(signInSaga)]);
}
