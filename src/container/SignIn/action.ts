import { AuthenticationActions } from "./type";

export const actionCreators = {
  signIn: (data: any) => ({
    type: AuthenticationActions.SIGN_IN,
    payload: data,
  }),
  setUser: (data: any) => ({
    type: AuthenticationActions.SET_USER,
    payload: data,
  }),
};
