import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Row from "../Row";
import { actionCreators } from "../ClientForm/action";
import { useEffect } from "react";
export function createData(
  name: string,
  nip: string,
  telefonNumber: string | undefined,
  address: string,
  city: string
) {
  return {
    name,
    nip,
    telefonNumber,
    address,
    city,
    history: [
      {
        date: "2020-01-05",
        customerId: "11091700",
        amount: 3,
      },
      {
        date: "2020-01-02",
        customerId: "Anonymous",
        amount: 1,
      },
    ],
  };
}

const rows = [
  createData(
    "Intive GmbH",
    "PL1070043765",
    undefined,
    "Prinz-Ludwig-Str. 17",
    "Regensburg"
  ),
  createData(
    "Hanus Commercial Arts - Konrad Hanus",
    "8982076947",
    "669975140",
    "Piastwoska 38/20",
    "Wrocław"
  ),
  createData(
    "Mateusz Gibuła Bakkchos",
    "8691992450",
    "781259942",
    "Niedzieliska 283",
    "Szczurowa"
  ),
];

interface Props {
  list: Array<any>;
}
type Actions = typeof actionCreators;

export default function ClientsPage(props: Props & Actions) {
  useEffect(() => {
    props.getClients();
  }, []);

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Klient</TableCell>
            <TableCell align="right">NIP</TableCell>
            <TableCell align="right">Telefon</TableCell>
            <TableCell align="right">Adres</TableCell>
            <TableCell align="right">Miasto</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.list.map((row) => (
            <Row key={row.purchaserNip} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
