import { connect } from "react-redux";
import ClientTable from "./ClientTable";
import { actionCreators } from "../ClientForm/action";
function mapStateToProps({ clientForm }) {
  return { list: clientForm.list };
}

export default connect(mapStateToProps, actionCreators)(ClientTable);
