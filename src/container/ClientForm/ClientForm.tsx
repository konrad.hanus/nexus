import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { useState } from "react";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import React from "react";
import { actionCreators } from "./action";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  marginTop: 15,
  color: theme.palette.text.secondary,
}));

interface Props {
  clientForm: any;
}
type Actions = typeof actionCreators;

function ClientForm(props: Props & Actions) {
  const [client, setClient] = useState({});
  const handleChange = (e) => {
    const name = e.target.id;
    const value = e.target.value;
    let tmp = { ...client };
    tmp[name] = value;
    setClient(tmp);
  };
  const handleClick = () => {
    props.saveNewClient(client);
  };
  return (
    <Container maxWidth="sm">
      <Item>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            Nabywca
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserName"
              label="Nazwa firmy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserAddress"
              label="Adres firmy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserNip"
              label="NIP"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserCode"
              label="Kod pocztowy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserCity"
              label="Miasto"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="purchaserPhone"
              label="Telefon"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
        </Grid>
      </Item>

      <Item>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            Adresat
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="addresseeName"
              label="Nazwa firmy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="addresseeName"
              label="Adres firmy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="addresseeCode"
              label="Kod pocztowy"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="addresseeCity"
              label="Miasto"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="addresseePhone"
              label="Telefon"
              variant="outlined"
              onChange={handleChange}
            />
          </Grid>
        </Grid>
      </Item>
      <Grid item xs={12} sm={6}>
        <Button variant="contained" onClick={handleClick}>
          Zapisz
        </Button>
      </Grid>
    </Container>
  );
}

export default ClientForm;
