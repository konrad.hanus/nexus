import { connect } from "react-redux";
import { actionCreators } from "./action";
import ClientForm from "./ClientForm";
function mapStateToProps({ clientForm }) {
  return { clientForm };
}

export default connect(mapStateToProps, actionCreators)(ClientForm);
