import { CLIENT_ACTIONS } from "./type";

const reducer = (state = { list: [] }, action) => {
  switch (action.type) {
    case CLIENT_ACTIONS.SET_ALL:
      return {
        ...state,
        list: [...Object.values(action.payload)],
      };
      break;
    default:
      return state;
  }
};

export default reducer;
