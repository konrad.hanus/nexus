import { all, takeEvery, fork, call, put } from "redux-saga/effects";
import { reduxSagaFirebase } from "../../firebase";
import { CLIENT_ACTIONS } from "./type";
import { actionCreators } from "./action";

function* onClientSaveSaga(action) {
  yield call(reduxSagaFirebase.database.create, "client", action.payload);
}
function* saveSaga() {
  yield takeEvery(CLIENT_ACTIONS.CREATE, onClientSaveSaga);
}

function* onGetClientSaga() {
  const data = yield call(reduxSagaFirebase.database.read, "client");
  yield put(actionCreators.setClients(data));
}
function* getClientSaga() {
  yield takeEvery(CLIENT_ACTIONS.GET_ALL, onGetClientSaga);
}

export function* clientFormSaga() {
  yield all([fork(saveSaga), fork(getClientSaga)]);
}
