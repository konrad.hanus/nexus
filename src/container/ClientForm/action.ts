import { CLIENT_ACTIONS } from "./type";
export const actionCreators = {
  saveNewClient: (data: any) => ({
    type: CLIENT_ACTIONS.CREATE,
    payload: data,
  }),
  getClients: () => ({
    type: CLIENT_ACTIONS.GET_ALL,
  }),
  setClients: (data: any) => ({
    type: CLIENT_ACTIONS.SET_ALL,
    payload: data,
  }),
};
