// Import the functions you need from the SDKs you need
import firebase from "firebase";
import ReduxSagaFirebase from "redux-saga-firebase";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  FirebaseConfig: {
    apiKey: "AIzaSyCswxwnaI4bg-ZZLeuPqQ--xCuZQbnk_sE",
    authDomain: "reactnexus.firebaseapp.com",
    databaseURL:
      "https://reactnexus-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "reactnexus",
    storageBucket: "reactnexus.appspot.com",
    messagingSenderId: "528565469019",
    appId: "1:528565469019:web:db5711103dba503c42a47e",
    measurementId: "G-LLYVLRLGTJ",
  },
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig.FirebaseConfig);
const reduxSagaFirebase = new ReduxSagaFirebase(app);
export { reduxSagaFirebase };
