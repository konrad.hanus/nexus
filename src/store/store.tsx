import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./rootReducer";
import { compose } from "redux";
// @ts-ignore: REDUX DEV TOOLS
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const sagaMiddleware = createSagaMiddleware();

const middleware = composeEnhancers(applyMiddleware(sagaMiddleware));

const configureStore = () => createStore(rootReducer(), middleware);

export default configureStore;
