import { combineReducers } from "redux";
import invoiceFormReducer from "../container/InvoiceForm/reducer";
import clientFormReducer from "../container/ClientForm/reducer";
import userReducer from "../container/SignIn/reducer";
import dataReducer from "../container/DataForm/reducer";

const rootReducer = () =>
  combineReducers({
    invoiceForm: invoiceFormReducer,
    clientForm: clientFormReducer,
    user: userReducer,
    data: dataReducer,
  });

export default rootReducer;
