import { runSaga } from "@redux-saga/core";
import { takeEvery } from "redux-saga/effects";
import { reduxSagaFirebase } from "../../../firebase";
import { actionCreators } from "../action";
import { invoicesSaga, onInvoicesSaga } from "../saga";
import { INVOICE_PAGE_ACTIONS } from "../type";
import invoiceFactory from "../../../tests/factory/InvoiceFactory";

describe("InvoicePageSaga testing", () => {
  const saga = invoicesSaga();
  it("should wait for every GET_ALL_INVOICES action and call onInvoiceSaga", () => {
    expect(saga.next().value).toEqual(
      takeEvery(INVOICE_PAGE_ACTIONS.GET_ALL_INVOICES, onInvoicesSaga)
    );
  });

  it("should be done", () => {
    expect(saga.next().done).toBeTruthy();
  });

  it("should receive invoices", async () => {
    const invoices = invoiceFactory.buildList(5);

    const read = jest
      .spyOn(reduxSagaFirebase.database, "read")
      .mockImplementation((): any => invoices);

    const dispatched = [];
    const result = await runSaga(
      {
        dispatch: (action: never) => dispatched.push(action),
      },
      onInvoicesSaga
    );

    expect(read).toHaveBeenCalledTimes(1);
    expect(dispatched[0]).toEqual(actionCreators.setInvoices(invoices));
  });
});
