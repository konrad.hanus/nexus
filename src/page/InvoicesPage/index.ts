import { connect } from "react-redux";
import InvoicesPage from "./InvoicesPage";
import { actionCreators } from "./action";

function mapStateToProps(state) {
  const { invoiceForm } = state;
  return { list: invoiceForm.list };
}

export default connect(mapStateToProps, actionCreators)(InvoicesPage);
