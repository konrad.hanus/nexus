import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { useEffect } from "react";
import { connect } from "react-redux";
import { actionCreators } from "./action";
import { useHistory } from "react-router-dom";

type Props = { list: Array<any> } & typeof actionCreators;

export default function InvoicesPage(props: Props) {
  let history = useHistory();
  useEffect(() => {
    props.getInvoices();
  }, []);

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Number</TableCell>
            <TableCell align="right">Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.list.map((row) => (
            <>
              <div
                onClick={() => {
                  history.push(`/invoice/${row.number}`);
                }}
              >
                <TableRow
                  key={row.number}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.number}
                  </TableCell>
                  <TableCell align="right">{row.date}</TableCell>
                </TableRow>
              </div>
            </>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
