import { takeEvery, all, fork, call, put } from "redux-saga/effects";
import { reduxSagaFirebase } from "../../firebase";
import { INVOICE_PAGE_ACTIONS } from "../../page/InvoicesPage/type";
import { actionCreators } from "./action";

export function* onInvoicesSaga() {
  const data = yield call(reduxSagaFirebase.database.read, "invoice");
  yield put(actionCreators.setInvoices(data));
}

export function* invoicesSaga() {
  yield takeEvery(INVOICE_PAGE_ACTIONS.GET_ALL_INVOICES, onInvoicesSaga);
}

export function* invoicePageSaga() {
  yield all([fork(invoicesSaga)]);
}
