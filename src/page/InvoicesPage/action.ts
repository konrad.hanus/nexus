import { Invoice } from "../../container/InvoiceForm/type";
import { GetInvoicesAction, INVOICE_PAGE_ACTIONS } from "./type";

export const actionCreators = {
  getInvoices: (): GetInvoicesAction => ({
    type: INVOICE_PAGE_ACTIONS.GET_ALL_INVOICES,
    payload: null,
  }),
  setInvoices: (list: Invoice[]) => ({
    type: INVOICE_PAGE_ACTIONS.SET_INVOICES,
    payload: list,
  }),
};
