export enum INVOICE_PAGE_ACTIONS {
  GET_ALL_INVOICES = "@INVOICE_PAGE_ACTION/GET_ALL",
  SET_INVOICES = "@INVOICE_PAGE_ACTION/SET_ALL",
}
export interface GetInvoicesAction {
  type: INVOICE_PAGE_ACTIONS.GET_ALL_INVOICES;
  payload: null;
}
