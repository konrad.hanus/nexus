import { useParams } from "react-router";
import InvoiceForm from "../container/InvoiceForm";
import DataForm from "../container/DataForm";
import ClientForm from "../container/ClientForm";
import { RouteParams } from "../types/router.types";

const renderByPage = (param: string) => {
  switch (param) {
    case "invoices":
      return <InvoiceForm />;

    case "clients":
      return <ClientForm />;

    case "data":
      return <DataForm />;

    default:
      return null;
  }
};

export default function NewPage() {
  const { page } = useParams() as RouteParams;
  return renderByPage(page);
}
