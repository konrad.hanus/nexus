import { connect, useStore } from "react-redux";
import { useParams } from "react-router";
import { Invoice } from "../container/InvoiceForm/type";
import { RouteParams } from "../types/router.types";

export default function InvoicePage() {
  const { number } = useParams() as RouteParams;
  const store = useStore();
  const list = store.getState().invoiceForm.list;
  const invoice: Invoice = list.find((element) => element.number === number);

  return (
    <>
      <p>{invoice.number}</p>
    </>
  );
}
